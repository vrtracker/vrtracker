﻿using Assets.Scripts.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainScript : MonoBehaviour
{
    private SerialPortHelper _serialPortHelper;

    private bool _connected;

    public int EyeSensivityLeft = 15;
    public int EyeSensivityRight = 5;
    private bool _end;
    public Thread mThread;
    private float _oldTime;

    public float[] MinStartTimes;
    public float[] MaxEndTimes;
    void Start()
    {
        //SerialPortListener.OpenPort();
        _oldTime = DateTime.Now.Second;
        _serialPortHelper = new SerialPortHelper(EyeSensivityRight, EyeSensivityLeft);
        ThreadStart ts = UpdatePoint;
        mThread = new Thread(ts);
        mThread.Start();

        if (MinStartTimes.Length == MaxEndTimes.Length)
        {
            for (int i = 0; i < MaxEndTimes.Length; i++)
                GlobalVariables.TimeGroups.Add(new TimeGroup
                {
                    StartTime = MinStartTimes[i],
                    EndTime = MaxEndTimes[i]
                });
        }
        else
            Debug.LogError("Error in time groups");
    }

    void UpdatePoint()
    {
        while (DateTime.Now.Second - _oldTime < 5)
            _serialPortHelper.Calibration(SerialPortListener.GetMessage());

        _serialPortHelper.SetCenterAverage();

        while (true)
        {
            Vector3 point = _serialPortHelper.GetPoint(SerialPortListener.GetMessage());
            GlobalVariables.EyePosition = point;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(KeyCode.G))
            _serialPortHelper.SetDefaultCursorPosition();

        if (Time.time > GlobalVariables.TimeGroups[GlobalVariables.TimeGroups.Count-1].EndTime && !_end)
        {
            _end = true;
            mThread.Abort();
            SerialPortListener.ClosePort();
            foreach (var name in GlobalVariables.GameObjectToDetail)
                GameObject.Find(name).GetComponent<DetailObjectController>().OnRecordChanges();

            SceneManager.LoadScene("ResultScene");
        }
    }

    void OnApplicationQuit()
    {
        mThread.Abort();
        SerialPortListener.ClosePort();
    }
}
