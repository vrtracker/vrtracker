﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ButtonsOnClick : MonoBehaviour
{ 

    public void OnConnectPressed()
    {
        Debug.Log("try to connect...");

        if (!string.IsNullOrEmpty(GlobalVariables.ComPort))
        {
            SerialPortListener.OpenPort();
            var connectText = GameObject.Find("ConnectText").GetComponent<Text>();
            connectText.text = $"Status: connected to {GlobalVariables.ComPort}";
            SceneManager.LoadScene("MainScene");
            Debug.Log("Game started!");
        }
        else
        {
            Debug.Log("failed");
        }
    }
}
