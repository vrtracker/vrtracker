﻿using System.Collections;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
public class DropdownController : MonoBehaviour
{
    // Start is called before the first frame update

    public Dropdown Target;

    void Start()
    {
        //add com ports on start:
        Target.AddOptions(SerialPort.GetPortNames().ToList());
        
        //default com port:
        GlobalVariables.ComPort = SerialPort.GetPortNames()[0];
        Debug.Log(GlobalVariables.ComPort);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnItemClick()
    {
        Debug.Log($"port: {Target.options[Target.value].text}");
        GlobalVariables.ComPort = Target.options[Target.value].text;
    }
}
