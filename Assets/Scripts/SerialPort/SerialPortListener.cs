﻿using System;
using System.IO.Ports;
public static class SerialPortListener
{
    private static SerialPort _serialPort = new SerialPort();

    public static bool OpenPort()
    {
        try
        {
            _serialPort.PortName = GlobalVariables.ComPort;
            _serialPort.BaudRate = 9600;
            _serialPort.ReadTimeout = 10000;

            _serialPort.Open();

            _serialPort.WriteLine("e");
            return true;
        }
        catch
        {
            return false;
        }
    }

    public static string GetMessage()
    {
        return _serialPort.ReadLine();
    }

    public static void ClosePort()
    {
        _serialPort.Close();
    }

    /*
    public bool IsDeviceExists(out string deviceId)
    {
        //mono не может в system.management.dll


        deviceId = "";
        
        const string query = "SELECT * FROM Win32_SerialPort";
        var searcher = new ManagementObjectSearcher(query);

        foreach (var o in searcher.Get())
        {
            var service = (ManagementObject)o;
            if (service["Description"].ToString() != "Arduino UNO") continue;

            deviceId = service["DeviceID"].ToString();
            return true;
        }

        return false; 

    }*/
}
