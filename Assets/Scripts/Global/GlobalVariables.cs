﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Models;
using UnityEngine;

/// <summary>
/// class used for global variables
/// </summary>
public class GlobalVariables : MonoBehaviour
{
    public static string ComPort = "COM7";

    public static Vector3 EyePosition;

    public static string NowFocused = ""; 

    public static List<string> GameObjectToDetail = new List<string>();

    public static List<DataStatistics> DataToStatistics = new List<DataStatistics>();

    public static List<TimeGroup> TimeGroups = new List<TimeGroup>();
}
