﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Assets.Scripts.Models;
using TMPro.SpriteAssetUtilities;
using UnityEngine;
using UnityMeshSimplifier;

public class EyeController : MonoBehaviour
{
    public bool UseEye = false;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //TODO: remove legacy:
        if(!UseEye)
            GlobalVariables.EyePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y,0);

        transform.position = Camera.main.ScreenPointToRay(GlobalVariables.EyePosition).GetPoint(30);

        var ray = Camera.main.ScreenPointToRay(GlobalVariables.EyePosition);

        if (Physics.SphereCast(ray, 3f, out var hit))
        {
            var objectHit = hit.transform.gameObject;

            if (objectHit.name == GlobalVariables.NowFocused)
                return;

            var oldObjectToDetalize = GlobalVariables.GameObjectToDetail.Exists(e => e.Equals(GlobalVariables.NowFocused));
            var objectToDetalize = GlobalVariables.GameObjectToDetail.Exists(e => e.Equals(objectHit.name));

            var oldObjectController = GameObject.Find(GlobalVariables.NowFocused);
            var objectHitController = objectHit.GetComponent<DetailObjectController>();

            if (!objectToDetalize && oldObjectToDetalize)
            {
                //simplify old object
                oldObjectController.GetComponent<DetailObjectController>().OnOutFocus();
                GlobalVariables.NowFocused = objectHit.name;
            }
            else if (objectToDetalize && !oldObjectToDetalize)
            {
                objectHitController.OnFocus();
                GlobalVariables.NowFocused = objectHit.name;
            }
            else if (objectToDetalize)
            {
                objectHitController.OnFocus();
                oldObjectController.GetComponent<DetailObjectController>().OnOutFocus();
                GlobalVariables.NowFocused = objectHit.name;
            }
        }
        else if(!Physics.SphereCast(ray, 3f, out hit) && !string.IsNullOrEmpty(GlobalVariables.NowFocused))
        {
            //simplifing back
            if (!GlobalVariables.GameObjectToDetail.Exists(e => e.Equals(GlobalVariables.NowFocused)))
                return;
            
            GameObject.Find(GlobalVariables.NowFocused).GetComponent<DetailObjectController>().OnOutFocus();
            
            GlobalVariables.NowFocused = "";
        }
    }
}
