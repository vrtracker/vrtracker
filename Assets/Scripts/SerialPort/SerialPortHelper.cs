﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// class to get point from input raw data
/// first 100 values may be incorrect(filter butterworth)
/// </summary>
public class SerialPortHelper
{

    private double _centerLeftY = 510; //default
    private double _centerRightY = 510; //default

    private List<double> arrayCenterLeft = new List<double>();
    private List<double> arrayCenterRight = new List<double>();
    //center

    private List<double> eogLeft = new List<double>();
    private List<double> eogRight = new List<double>();

    private double oldRightChange;
    private double oldLeftChange;

    private int _positionX = 960, _positionY = 540;

    private readonly int _sensivityRight = 1;
    private readonly int _sensivityLeft = 2;
    public SerialPortHelper(int sensivityRight, int sensivityLeft)
    {
        _sensivityRight = sensivityRight;
        _sensivityLeft = sensivityLeft;
    }

    public void SetDefaultCursorPosition()
    {
        _positionY = 540;
        _positionX = 960;
    }

    public Vector3 GetPoint(string raw)
    {
        var (valueLeft,valueRight) = SplitSpace(raw);
        if (eogLeft.Count < 25)
        {
            eogLeft.Add(valueLeft);
            eogRight.Add(valueRight);
        }
        else
        {
            (eogLeft, eogRight) = ShiftDoublesToLeft(eogLeft, eogRight);
            eogLeft.Add(valueLeft);
            eogRight.Add(valueRight);

            var tempLeftFiltred = Butterworth(eogLeft.ToArray(), 0.0005, 100);
            var tempRightFiltred = Butterworth(eogRight.ToArray(), 0.0005, 100);

            var newRightChange = tempRightFiltred[23] - _centerRightY;
            var newLeftChange = tempLeftFiltred[23] - _centerLeftY;

            if (newRightChange > 40)
            {
                if (oldRightChange < newRightChange)
                    _positionX -= _sensivityRight * (int)(newRightChange - oldRightChange);

                oldRightChange = newRightChange;
            }

            if (newRightChange < -40)
            {
                if (oldRightChange < Math.Abs(newRightChange))
                    _positionX += _sensivityRight * (int)(Math.Abs(newRightChange) - oldRightChange);

                oldRightChange = Math.Abs(newRightChange);
            }

            if (newLeftChange > 10)
            {
                if (oldLeftChange < newLeftChange)
                    _positionY -= _sensivityLeft * (int)(newLeftChange - oldLeftChange);

                oldLeftChange = newLeftChange;
            }

            if (newLeftChange < -10)
            {
                if (oldLeftChange < Math.Abs(newLeftChange)) 
                    _positionY += _sensivityLeft * (int)(Math.Abs(newLeftChange) - oldLeftChange);
                

                oldLeftChange = Math.Abs(newLeftChange);
            }
            if (_positionX > 1800)
                _positionX = 1800;
            if (_positionX < 100)
                _positionX = 100;
            if (_positionY > 900)
                _positionY = 900;
            if (_positionY < 100)
                _positionY = 100;


        }
        return new Vector3(_positionX, _positionY);
    }

    private (List<double>, List<double>) ShiftDoublesToLeft(List<double> inputFirst, List<double> inputSecond)
    {
        var outputFirst = new List<double>();
        var outputSecond = new List<double>();
        for (var i = 0; i < inputFirst.Count - 1; i++)
        {
            outputFirst.Add(inputFirst[i + 1]);
            outputSecond.Add(inputSecond[i + 1]);
        }

        return (outputFirst, outputSecond);
    }

    public void Calibration(string raw)
    {
        var (valueLeft, valueRight) = SplitSpace(raw);

        arrayCenterLeft.Add(valueLeft);
        arrayCenterRight.Add(valueRight);
    }

    public void SetCenterAverage()
    {
        _centerLeftY = arrayCenterLeft.Average();
        _centerRightY = arrayCenterRight.Average();
        Debug.Log($"\n CenterLeft = {_centerLeftY} \t CenterRight = {_centerRightY}");
    }

    public (ushort, ushort) SplitSpace(string raw)
    {
        try
        {
            var parts = raw.Split(new[] { "\t" }, StringSplitOptions.RemoveEmptyEntries);
            return (Convert.ToUInt16(parts[0]), Convert.ToUInt16(parts[1]));
        }
        catch
        {
            return (0, 0);
        }
    }

    private double[] Butterworth(double[] indata, double deltaTimeinsec, double cutOff)
    {
        if (indata == null) return null;
        if (cutOff == 0) return indata;

        var Samplingrate = 1 / deltaTimeinsec;
        long dF2 = indata.Length - 1; // The data range is set with dF2
        var Dat2 = new double[dF2 + 4]; // Array with 4 extra points front and back
        var data = indata; // Ptr., changes passed data

        // Copy indata to Dat2
        for (long r = 0; r < dF2; r++)
            Dat2[2 + r] = indata[r];
        Dat2[1] = Dat2[0] = indata[0];
        Dat2[dF2 + 3] = Dat2[dF2 + 2] = indata[dF2];

        const double pi = 3.14159265358979;
        var wc = Math.Tan(cutOff * pi / Samplingrate);
        var k1 = 1.414213562 * wc; // Sqrt(2) * wc
        var k2 = wc * wc;
        var a = k2 / (1 + k1 + k2);
        var b = 2 * a;
        var c = a;
        var k3 = b / k2;
        var d = -2 * a + k3;
        var e = 1 - 2 * a - k3;

        // RECURSIVE TRIGGERS - ENABLE filter is performed (first, last points constant)
        var DatYt = new double[dF2 + 4];
        DatYt[1] = DatYt[0] = indata[0];
        for (long s = 2; s < dF2 + 2; s++)
            DatYt[s] = a * Dat2[s] + b * Dat2[s - 1] + c * Dat2[s - 2]
                       + d * DatYt[s - 1] + e * DatYt[s - 2];
        DatYt[dF2 + 3] = DatYt[dF2 + 2] = DatYt[dF2 + 1];

        // FORWARD filter
        var datZt = new double[dF2 + 2];
        datZt[dF2] = DatYt[dF2 + 2];
        datZt[dF2 + 1] = DatYt[dF2 + 3];
        for (var t = -dF2 + 1; t <= 0; t++)
            datZt[-t] = a * DatYt[-t + 2] + b * DatYt[-t + 3] + c * DatYt[-t + 4]
                        + d * datZt[-t + 1] + e * datZt[-t + 2];


        // Calculated points copied for return
        for (long p = 0; p < dF2; p++)
            data[p] = datZt[p];

        return data;
    }

}
