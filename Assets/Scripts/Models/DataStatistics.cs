﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Models
{
    public class DataStatistics
    {
        public string Name { get; set; }
        public List<float> StartTimesList { get; set; }
        public List<float> EndTimesList { get; set; }
        public float MinWatchTime { get; set; }
        public float MaxWatchTime { get; set; }
        public int TimeGroup { get; set; }
    }
}
