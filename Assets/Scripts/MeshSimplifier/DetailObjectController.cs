﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Assets.Scripts.Models;
using UnityEditor;
using UnityEngine;
using UnityMeshSimplifier;

public class DetailObjectController : MonoBehaviour
{
    [HideInInspector] public Mesh SourceMesh;

    [Range(0f, 1f)] 
    public float Quality = 0.3f;

    public int TimeGroup = 0;
    public float MinWatchTime = 0.1f;
    public float MaxWatchTime = 10;

    private DataStatistics _data;

    private Material _highlightMaterial;
    void Start()
    {
        _highlightMaterial = Resources.Load<Material>("Materials/HoverHighlight");
        _data = new DataStatistics();
        _data.StartTimesList = new List<float>();
        _data.EndTimesList = new List<float>();
        _data.Name = name;
        _data.TimeGroup = TimeGroup;
        _data.MinWatchTime = MinWatchTime;
        _data.MaxWatchTime = MaxWatchTime;
        SourceMesh = GetComponent<MeshFilter>().mesh;
        GlobalVariables.GameObjectToDetail.Add(name);
        StartCoroutine(ObjectDetalization(0.3f));
    }

    //TODO: change to events
    public void OnFocus()
    {
        _data.StartTimesList.Add(Time.time);

        StartCoroutine(ObjectDetalization(1f));
        Debug.Log($"Focused: {name}");

        var tempMat = GetComponent<MeshRenderer>().materials;
        tempMat[tempMat.Length-1] = _highlightMaterial;
        GetComponent<MeshRenderer>().materials = tempMat;
    }

    public void OnOutFocus()
    {
        _data.EndTimesList.Add(Time.time);

        StartCoroutine(ObjectDetalization(0.3f));
        Debug.Log(Time.time - _data.StartTimesList.ToArray().Last());
        Debug.Log($"Unfocused: {name}");

        var tempMat = GetComponent<MeshRenderer>().materials;
        tempMat[tempMat.Length - 1] = null;
        GetComponent<MeshRenderer>().materials = tempMat;
    }
    private IEnumerator ObjectDetalization(float quality)
    {
        var meshSimplifier = new MeshSimplifier(SourceMesh);
        meshSimplifier.SimplifyMesh(quality);
        var destMesh = meshSimplifier.ToMesh();
        GetComponent<MeshFilter>().mesh = destMesh;
        GetComponent<MeshCollider>().sharedMesh = destMesh;
        Quality = quality;
        yield return null;
    }

    public void OnRecordChanges()
    {
        GlobalVariables.DataToStatistics.Add(_data);
    }
}
