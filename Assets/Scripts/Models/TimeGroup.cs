﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Models
{
    public class TimeGroup
    {
        public float StartTime { get; set; }
        public float EndTime { get; set; }
    }
}
